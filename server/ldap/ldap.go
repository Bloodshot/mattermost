package ldap

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/go-ldap/ldap/v3"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/shared/mlog"

	"github.com/mattermost/mattermost/server/v8/channels/app/request"
	"github.com/mattermost/mattermost/server/v8/channels/app/users"
	"github.com/mattermost/mattermost/server/v8/channels/app/platform"
	"github.com/mattermost/mattermost/server/v8/einterfaces"
	ejobs "github.com/mattermost/mattermost/server/v8/einterfaces/jobs"
	"github.com/mattermost/mattermost/server/v8/channels/jobs"
	"github.com/mattermost/mattermost/server/v8/channels/store"
)

type LDAPClient struct {
	store *store.Store
	settings *model.LdapSettings
	users **users.UserService
  platform **platform.PlatformService
	jobs **jobs.JobServer
	pictureService PictureService
  logger mlog.LoggerIFace
}

type PictureService interface {
	SetProfileImageFromFile(context request.CTX, userID string, file io.Reader) *model.AppError
}

type LdapInterface interface {
	einterfaces.LdapInterface
	ejobs.LdapSyncInterface
}

func NewLdapInterface(settings *model.LdapSettings,
                      users **users.UserService, jobs **jobs.JobServer,
                      platform **platform.PlatformService,
											pictureService PictureService,
                      logger mlog.LoggerIFace) LdapInterface {
	return &LDAPClient{ &((**platform).Store), settings, users, platform, jobs, pictureService, logger }
}


func (lp *LDAPClient) connect() (conn *ldap.Conn, err error) {
	ldap.DefaultTimeout = time.Duration(*lp.settings.QueryTimeout) * time.Second
	if *lp.settings.ConnectionSecurity == "TLS" {
		conn, err = ldap.DialTLS("tcp",
		                         fmt.Sprintf("%s:%d", *lp.settings.LdapServer, *lp.settings.LdapPort),
		                         &tls.Config{ InsecureSkipVerify: *lp.settings.SkipCertificateVerification,
		                         ServerName: *lp.settings.LdapServer })
	} else {
		conn, err = ldap.Dial("tcp", fmt.Sprintf("%s:%d", *lp.settings.LdapServer, *lp.settings.LdapPort))
	}
	return
}

func (lp *LDAPClient) connectSocket() (conn *ldap.Conn, err error) {
	conn, err = ldap.DialURL(fmt.Sprintf("ldapi://%s", *lp.settings.LdapServer))
	return
}

func (lp *LDAPClient) bindRoot(conn *ldap.Conn) (err error) {
	err = conn.Bind(*lp.settings.BindUsername, *lp.settings.BindPassword)
	return
}

func (lp *LDAPClient) bindSocket(conn *ldap.Conn) (err error) {
	err = conn.ExternalBind()
	return
}

func (lp *LDAPClient) doConnect() (conn *ldap.Conn, err error) {
	socket := strings.HasPrefix(*lp.settings.LdapServer, "/")

	if socket {
		conn, err = lp.connectSocket()
	} else {
		conn, err = lp.connect()
	}

	if err != nil {
		return nil, model.NewAppError("doConnect", "ent.ldap.do_login.unable_to_connect.app_error", nil, err.Error(), http.StatusInternalServerError)
	}

	if socket {
		err = lp.bindSocket(conn)
	} else {
		err = lp.bindRoot(conn)
	}

	if err != nil {
		return nil, model.NewAppError("doConnect","ent.ldap.do_login.bind_admin_user.app_error", nil, err.Error(), http.StatusInternalServerError)
		conn.Close()
		return
	}
	return
}

func escapeUIDs(uids []string) (escaped []string) {
	for _, uid := range uids {
		escaped = append(escaped, ldap.EscapeFilter(uid))
	}
	return
}

func (lp *LDAPClient) entryToUser(entry *ldap.Entry) (user *model.User, authData string) {
	authData = entry.GetAttributeValue(*lp.settings.LoginIdAttribute)
	user = &model.User{
		AuthService: model.UserAuthServiceLdap,
	  FirstName:   entry.GetAttributeValue(*lp.settings.FirstNameAttribute),
		LastName:    entry.GetAttributeValue(*lp.settings.LastNameAttribute),
		Email:	     entry.GetAttributeValue(*lp.settings.EmailAttribute),
		Username:    entry.GetAttributeValue(*lp.settings.UsernameAttribute),
		AuthData:    &authData,
		Nickname:    entry.GetAttributeValue(*lp.settings.NicknameAttribute),
		Position:    entry.GetAttributeValue(*lp.settings.PositionAttribute),
	}
	return
}

func (lp *LDAPClient) updateUserFromLdapUser(user *model.User, ldapUser *model.User) error {
	if (user.FirstName != ldapUser.FirstName ||
	    user.LastName != ldapUser.LastName ||
	    user.Email != ldapUser.Email ||
	    user.Nickname != ldapUser.Nickname ||
	    user.Position != ldapUser.Position) {

		user.FirstName = ldapUser.FirstName
		user.LastName = ldapUser.LastName
		user.Email = ldapUser.Email
		user.Nickname = ldapUser.Nickname
		user.Position = ldapUser.Position

		_, err := (**lp.users).UpdateUser(user, true)
		return err
	}

	return nil
}

func (lp *LDAPClient) getUsersByUIDs(uids []string) (conn *ldap.Conn, entries map[string]*ldap.Entry, users map[string]*model.User, err error) {
	conn, err = lp.doConnect()
	if err != nil {
		return
	}

	// there is no limit for ldap query string
	// let's use users filter instead of retrieving all LDAP users
	uidsFilter := fmt.Sprintf(
		"|(%s=%s)",
		*lp.settings.LoginIdAttribute,
		strings.Join(
			escapeUIDs(uids),
			fmt.Sprintf(")(%s=",
				*lp.settings.LoginIdAttribute)))

	result, err := conn.Search(&ldap.SearchRequest{
		BaseDN: *lp.settings.BaseDN,
		Scope:	ldap.ScopeWholeSubtree,
		Filter: fmt.Sprintf("(&(%s)%s)", uidsFilter, *lp.settings.UserFilter)})

	if err != nil {
		conn.Close()
		err = model.NewAppError("doConnect","ent.ldap.do_login.search_ldap_server.app_error", nil, err.Error(), http.StatusInternalServerError)
		return
	}
	users = make(map[string]*model.User, len(result.Entries))
	entries = make(map[string]*ldap.Entry, len(result.Entries))

	for _, entry := range result.Entries {
		user, authData := lp.entryToUser(entry)
		users[authData] = user
		entries[authData] = entry
	}

	return
}

func (lp *LDAPClient) getUserByUID(uid string) (conn *ldap.Conn, entry *ldap.Entry, user *model.User, err *model.AppError) {
	conn, entries, users, _ := lp.getUsersByUIDs([]string{uid})
	if len(entries) > 1 {
		conn.Close()
		return nil, nil, nil, model.NewAppError("ldap.DoLogin", "ent.ldap.do_login.matched_to_many_users.app_error", nil, "", http.StatusInternalServerError)
	} else if len(entries) < 1 {
		return nil, nil, nil, model.NewAppError("ldap.DoLogin", "ent.ldap.do_login.user_not_registered.app_error", nil, "", http.StatusInternalServerError)
	}
	for _, user := range users {
		return conn, entries[*user.AuthData], user, nil
	}

	return
}

func (lp *LDAPClient) DoLogin(c *request.Context, id string, password string) (user *model.User, errApp *model.AppError) {
	conn, entry, user, errAp := lp.getUserByUID(id)

	if errAp != nil {
		return nil, errAp
	}

	defer conn.Close()

	err := conn.Bind(entry.DN, password)
	if err != nil {
		mlog.Debug("DoLogin", mlog.Err(err))
		return nil, model.NewAppError("ldap.DoLogin", "ent.ldap.do_login.invalid_password.app_error", nil, "", http.StatusInternalServerError)
	}

	if !(*lp.users).IsUsernameTaken(user.Username) {
		ruser, errApp := (*lp.users).CreateUser(user, users.UserCreateOptions { false, false })
		if errApp != nil {
			mlog.Debug("DoLogin", mlog.Err(errApp), mlog.String("email", user.Email))
			return nil, model.NewAppError("ldap.DoLogin", "ent.ldap.do_login.unable_to_create_user.app_error", nil, "", http.StatusInternalServerError)
		}
		return ruser, nil
	} else {
		user, err := (*lp.store).User().GetByUsername(*user.AuthData)
		if err != nil {
			mlog.Debug("DoLogin", mlog.Err(err))
			return nil, model.NewAppError("ldap.DoLogin", "ent.ldap.do_login.unable_to_create_user.app_error", nil, "", http.StatusInternalServerError)
		} else {
			return user, nil
		}
	}

	return user, nil
}

func (lp *LDAPClient) GetUser(id string) (*model.User, *model.AppError) {
	mlog.Debug("Started GetUser")
	conn, _, user, err := lp.getUserByUID(id)

	if err != nil {
		return nil, model.NewAppError("ldap.GetUser", "Cannot get user from LDAP", nil, "", http.StatusInternalServerError)
	}

	conn.Close()

	login, geterr := (*lp.store).User().GetByUsername(*user.AuthData)
	if geterr != nil {
		return user, nil
	} else {
		return login, nil
	}
}

func (lp *LDAPClient) CheckPassword(id string, password string) *model.AppError {
	return model.NewAppError("CheckPassword", "", nil, "", http.StatusInternalServerError)
	mlog.Debug("Started CheckPassword")
	conn, entry, _, errAp := lp.getUserByUID(id)
	if errAp != nil {
		return errAp
	}
	defer conn.Close()

	err := conn.Bind(entry.DN, password)
	if err != nil {
		return model.NewAppError("ldap.CheckPassword", "ent.ldap.do_login.invalid_password.app_error", nil, err.Error(), http.StatusInternalServerError)
	}

	return nil
}

func (lp *LDAPClient) SwitchToLdap(userId, ldapId, ldapPassword string) *model.AppError {
	// called from api/user.go:emailToLdap
	mlog.Debug("Started SwitchToLdap")

	conn, _, ldapUser, err := lp.getUserByUID(ldapId)

	if err != nil {
		return model.NewAppError("ldap.SwitchToLdap", "Cannot get user from LDAP", nil, "", http.StatusInternalServerError)
	}

	defer conn.Close()

	err = lp.CheckPassword(ldapId, ldapPassword)
	if err != nil {
		return err
	}
	_, storeErr := (*lp.store).User().UpdateAuthData(userId, model.UserAuthServiceLdap, ldapUser.AuthData, ldapUser.Email, false)
	if storeErr != nil {
		return model.NewAppError("ldap.SwitchToLdap", "ent.ldap.do_login.unable_to_create_user.app_error", nil, "", http.StatusInternalServerError)
	}
	return nil
}

func (lp *LDAPClient) ValidateFilter(filter string) *model.AppError {
	mlog.Debug("Started ValidateFilter")
	_, err := ldap.CompileFilter(filter)
	if err != nil {
		return model.NewAppError("ldap.ValidateFilter", "ent.ldap.validate_filter.app_error", nil, err.Error(), http.StatusInternalServerError)
	}
	return nil
}

func (lp *LDAPClient) StartSynchronizeJob(waitForJobToFinish bool, includeRemovedMembers bool) (*model.Job, *model.AppError){
	mlog.Debug("ldap.StartSynchronizeJob", mlog.String("jobName", jobName))
	return (**lp.jobs).CreateJob(model.JobTypeLdapSync, make(map[string]string))
}

func (lp *LDAPClient) updateActive(user *model.User, active bool) *model.AppError {
	if active {
		user.DeleteAt = 0
	} else {
		user.DeleteAt = model.GetMillis()
	}

	_, err := (*lp.users).UpdateUser(user, true)
	if err != nil {
		return model.NewAppError("ldap.updateActive", "", nil, "", http.StatusInternalServerError)
	} else {
		if user.DeleteAt > 0 {
			err = (*lp.platform).RevokeAllSessions(user.Id)
			if err != nil {
				return model.NewAppError("ldap.updateActive", "", nil, "", http.StatusInternalServerError)
			}
		}
	}
	return nil
}

func (lp *LDAPClient) Synchronize() *model.AppError {
  profiles, err := (*lp.store).User().GetAllUsingAuthService(model.UserAuthServiceLdap)
	var uids []string

	if err != nil {
		return model.NewAppError("ldap.Synchronize", "", nil, "", http.StatusInternalServerError)
	} else {
		for _, user := range profiles {
			uids = append(uids, *user.AuthData)
		}
		conn, entries, ldusers, err := lp.getUsersByUIDs(uids)
		if err != nil {
			return model.NewAppError("ldap.Synchronize", "ent.ldap.syncronize.get_all.app_error", nil, "", http.StatusInternalServerError)
		}
		conn.Close()
		for _, user := range profiles {
			if lduser, ok := ldusers[*user.AuthData]; !ok {
				if user.DeleteAt == 0 {
					mlog.Debug("ldap.Synchronize: User should be deactivated", mlog.String("authData", *user.AuthData))
					errAp := lp.updateActive(user, false)
					if errAp != nil {
						return errAp
					}
				}
			} else {
				if user.DeleteAt != 0 {
					mlog.Debug("ldap.Synchronize: User should be activated", mlog.String("authData", *user.AuthData))
					errAp := lp.updateActive(user, true)
					if errAp != nil {
						return errAp
					}
				}

				lp.updateProfilePictureIfNecessary(request.EmptyContext(lp.logger), user, entries[*user.AuthData])
				lp.updateUserFromLdapUser(user, lduser)
			}
		}

		return nil
	}
	return model.NewAppError("doConnect","ent.ldap.syncdone.info", nil, err.Error(), http.StatusInternalServerError)
}

func (lp *LDAPClient) SyncNow() {
	lp.Synchronize()
}

// RunTest does a test for making a simple connection.
func (lp *LDAPClient) RunTest() *model.AppError {
	conn, err := lp.doConnect()
	if err != nil {
		return model.NewAppError("ldap.RunTest", "ent.ldap.do_login.unable_to_connect.app_error", nil, "", http.StatusInternalServerError)
	}
	defer conn.Close()

	return nil
}

// GetAllLdapUsers() retrieves a list of all LDAP users.
func (lp *LDAPClient) GetAllLdapUsers() ([]*model.User, *model.AppError) {
	mlog.Debug("Started GetAllLdapUsers")
	conn, err := lp.doConnect()
	if err != nil {
		return nil, model.NewAppError("ldap.GetAllLdapUsers", "ent.ldap.getallldapusers.get_all.app_error", nil, "", http.StatusInternalServerError)
	}
	defer conn.Close()

	// retrieve all user entries
	result, err := conn.Search(&ldap.SearchRequest{
		BaseDN: *lp.settings.BaseDN,
		Scope:	ldap.ScopeWholeSubtree})
	if err != nil {
		return nil, model.NewAppError("doConnect","ent.ldap.getallldapusers.get_all.app_error", nil, err.Error(), http.StatusInternalServerError)
	}
	entries := result.Entries
	users := make([]*model.User, len(entries))

	for _, entry := range entries {
		user, _ := lp.entryToUser(entry)
		users = append(users, user)
	}

	return users, nil
}

func (lp *LDAPClient) MigrateIDAttribute(toAttribute string) error {
	mlog.Warn("MigrateIDAttribute")
	return model.NewAppError("MigrateIDAttribute", "", nil, "", http.StatusInternalServerError)
}

func (lp *LDAPClient) GetGroup(groupUID string) (*model.Group, *model.AppError) {
	mlog.Warn("GetGroup")
	return nil, model.NewAppError("GetGroup", "", nil, "", http.StatusInternalServerError)
}

func (lp *LDAPClient) GetAllGroupsPage(page int, perPage int, opts model.LdapGroupSearchOpts) ([]*model.Group, int, *model.AppError) {
	mlog.Warn("GetAllGroupsPage")
	return nil, 0, model.NewAppError("GetAllGroupsPage", "", nil, "", http.StatusInternalServerError)
}

func (lp *LDAPClient) FirstLoginSync(c *request.Context, user *model.User, userAuthService, userAuthData, email string) *model.AppError {
	mlog.Warn("FirstLoginSync")
	return model.NewAppError("FirstLoginSync", "", nil, "", http.StatusInternalServerError)
}

func (lp *LDAPClient) updateProfilePictureIfNecessary(ctx request.CTX, user *model.User, entry *ldap.Entry) {
	ldapPfp := entry.GetRawAttributeValue(*lp.settings.PictureAttribute)
	if len(ldapPfp) == 0 {
		return
	}

	currentPfp, defaultPfp, pfpErr := (*lp.users).GetProfileImage(user)

	if (pfpErr != nil) || defaultPfp || (bytes.Compare(ldapPfp, currentPfp) != 0) {
		io := bytes.NewReader(ldapPfp)
		lp.pictureService.SetProfileImageFromFile(ctx, user.Id, io)
	}
}

func (lp *LDAPClient) UpdateProfilePictureIfNecessary(ctx request.CTX, user model.User, _ model.Session) {
	conn, entry, _, err := lp.getUserByUID(*user.AuthData)
	if err != nil {
		return
	}

	lp.updateProfilePictureIfNecessary(ctx, &user, entry)
	conn.Close()
}

func (lp *LDAPClient) GetADLdapIdFromSAMLId(authData string) string {
	mlog.Warn("GetADLdapIdFromSAMLId")
	return ""
}

func (lp *LDAPClient) GetSAMLIdFromADLdapId(authData string) string {
	mlog.Warn("GetSAMLIdFromADLdapId")
	return ""
}

func (lp *LDAPClient) GetVendorNameAndVendorVersion() (string, string) {
	return "Rainbow Coalition", "0.1"
}

func (lp *LDAPClient) CheckPasswordAuthData(authData string, password string) *model.AppError {
	return model.NewAppError("CheckPasswordAuthData", "", nil, "", http.StatusInternalServerError)
}

func (lp *LDAPClient) CheckProviderAttributes(_ *model.LdapSettings, ouser *model.User, patch *model.UserPatch) string {
	mlog.Warn("CheckProviderAttributes")
	return ""
}

func (lp *LDAPClient) GetUserAttributes(id string, attributes []string) (map[string]string, *model.AppError) {
	mlog.Warn("GetUserAttributes")
	return nil, model.NewAppError("GetUserAttributes", "", nil, "", http.StatusInternalServerError)
}
